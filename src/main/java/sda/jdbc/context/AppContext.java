package sda.jdbc.context;

import sda.jdbc.repository.UczenRepository;
import sda.jdbc.service.EntityManagerProvider;
import sda.jdbc.service.UczenService;

public class AppContext {

    private final EntityManagerProvider entityManagerProvider;
    private final UczenService uczenService;
    private final UczenRepository uczenRepository;

    public AppContext() {
        entityManagerProvider = new EntityManagerProvider();

        uczenRepository = new UczenRepository(entityManagerProvider);

        uczenService = new UczenService(uczenRepository, entityManagerProvider);
    }

    public UczenService getUczenService() {
        return uczenService;
    }

    public void finish() {
        entityManagerProvider.closeJpa();
    }
}
