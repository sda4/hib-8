package sda.jdbc.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;

public class EntityManagerProvider {
    private final EntityManagerFactory emf;
    private  EntityManager em;

    public EntityManagerProvider() {
        emf = null;
    }

    public EntityManager startNewEm() {
        em = emf.createEntityManager();

        return em;
    }

    public EntityManager getCurrentEm() {
        return em;
    }

    public void startTransaction() {

    }

    public void commitTransaction() {

    }

    public void rollbackTransaction() {

    }

    public void endCurrentEm() {
        em.close();
        em = null;
    }

    public void clearEm() {

    }

    public void closeJpa() {
        emf.close();
    }
}
