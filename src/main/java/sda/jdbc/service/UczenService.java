package sda.jdbc.service;

import sda.jdbc.domain.Uczen;
import sda.jdbc.repository.UczenRepository;

import javax.persistence.EntityManagerFactory;

public class UczenService {

    private final UczenRepository uczenRepository;
    private final EntityManagerProvider entityManagerProvider;

    public UczenService(UczenRepository uczenRepository, EntityManagerProvider entityManagerProvider) {
        this.uczenRepository = uczenRepository;
        this.entityManagerProvider = entityManagerProvider;
    }

    public Uczen zapiszNowegoUczniaDoSzkoly(String imie, String nazwisko) {
        entityManagerProvider.startNewEm();
        entityManagerProvider.startTransaction();

        Uczen uczen = uczenRepository.create(new Uczen());

        entityManagerProvider.commitTransaction();
        entityManagerProvider.endCurrentEm();

        return uczen;
    }
}
