package sda.jdbc;

import lombok.extern.slf4j.Slf4j;
import sda.jdbc.context.AppContext;
import sda.jdbc.domain.Uczen;

@Slf4j
public class Main {

    // TODO - napisac prosta aplikacje do obslugi szkoly
    // encje: uczen, rodzic, nauczyciel, klasa, przedmiot, ocena


    private AppContext appContext;

    public static void main(String[] args) {
        new Main().runApp();
    }

    public void runApp() {
        appContext = new AppContext();

        // TODO: dopisac brakujace ciala metod, zeby zapis sie powiodl
        Uczen uczen = appContext.getUczenService().zapiszNowegoUczniaDoSzkoly("Szymon", "Dembek");

        appContext.finish();
    }
}
