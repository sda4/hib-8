package sda.jdbc.repository;

import sda.jdbc.domain.Uczen;
import sda.jdbc.service.EntityManagerProvider;

public class UczenRepository {

    private final EntityManagerProvider entityManagerProvider;

    public UczenRepository(EntityManagerProvider entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    public Uczen create(Uczen uczen) {
        return null;
    }

    public Uczen read(Long id) {
        return null;
    }

    public void update(Uczen uczen) {

    }

    public void delete(Uczen uczen) {

    }

    public void delete(long id) {

    }
}
